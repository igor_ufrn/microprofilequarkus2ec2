Framework para desenvolvimento de microsserviços.
Este projeto demonstra o uso, através de exemplos, de 
praticamente todo o microprofile implementado pelo Quarkus:

- Open Tracing
- Open API
- Rest Client
- Config
- Fault Tolerance
- Metrics
- JWT Propagation
- Health
- CDI
- JSON-P
- JAX-RS
- JSON-B
- Jakarta Annotation

Outras características:
- Uso do quarkus para enviar logs ao Splunk
- Pipeline definido no AWS code Pipeline: build, testes de unidade, testes do sonar, testes de performance, análise de vulnerabilidade
de container, deploy em EC2 (Evitar custos com deploy no EKS)
- arquitetura hexagonal
- CloudFormation para criar a máquina EC2 onde será realizado o deploy.
- Upload e download de arquivos com multipartFormData para economizar memória
