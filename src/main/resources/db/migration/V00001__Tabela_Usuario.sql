CREATE TABLE IF NOT EXISTS public.usuario
(
    login character varying(7) NOT NULL,
    senha character varying NOT NULL,
    nome character varying NOT NULL,
    perfil character varying NOT NULL,
    situacao "char" NOT NULL,
    ts_operacao timestamp without time zone NOT NULL
);

ALTER TABLE public.usuario
    OWNER to username;


insert into usuario VALUES ('igorbsb','password','Igor Linnik','ADMIN','A','01-01-2021')