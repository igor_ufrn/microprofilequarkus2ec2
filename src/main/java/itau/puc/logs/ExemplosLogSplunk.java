package itau.puc.logs;

import io.quarkus.arc.log.LoggerName;
import itau.puc.logs.dto.SplunkDTO;
import itau.puc.microprofile.restClient.PessoaDTO;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;

import javax.inject.Inject;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/exemplosLogSplunk")
@Tag(name = "ExemplosLogSplunk")
public class ExemplosLogSplunk {

    /**
     * O FQCN da classe declarante é usado como um nome de logger, ou seja, "ExemplosLogSplunk.class" será usado.
     *
     * As instâncias do logger são armazenadas em cache internamente.
     * Portanto, um LOG injetado em um bean @RequestScoped é compartilhado por todas as instâncias
     * de bean para evitar possível penalidade de desempenho associada à instanciação do logger.     *
     *
     */
    @Inject
    Logger LOG;

    /**
     * Neste caso, o nome foo é usado como um nome de logger, ou seja, "loggerName" será usado.
     */
    @LoggerName("loggerName")
    Logger loggerName;

    @Inject
    @RestClient
    SplunkHttpClient splunkHttpClient;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String logs() {
        LOG.info("ExemplosLogSplunk");
        loggerName.info("ExemplosLogSplunk");
        return "ExemplosLogSplunk";
    }
//    @POST
//    @Produces(MediaType.APPLICATION_JSON)
//    @Consumes(MediaType.APPLICATION_JSON)
//    public Response logSplunk(PessoaDTO pessoaDTO) {
//        SplunkDTO<PessoaDTO> pessoaDtoSplunk = new SplunkDTO<PessoaDTO>();
//        pessoaDtoSplunk.setEvent(pessoaDTO);
//        Response response = splunkHttpClient.adicionarJsonSplunk(pessoaDtoSplunk);
//        return response;
//    }
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response logSplunk(SplunkDTO<PessoaDTO> pessoaDtoSplunk) {
        return splunkHttpClient.adicionarJsonSplunk(pessoaDtoSplunk);
    }
}
