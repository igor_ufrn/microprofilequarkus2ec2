package itau.puc.logs;
import itau.puc.logs.dto.SplunkDTO;
import org.eclipse.microprofile.rest.client.annotation.ClientHeaderParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@ApplicationScoped
@RegisterRestClient(configKey = "splunk-server-endpoint")
@Path("/services/collector")
public interface SplunkHttpClient {

    @POST
    @Path("/event")
    @ClientHeaderParam(name = "Accept",         value = MediaType.APPLICATION_JSON)
    @ClientHeaderParam(name = "Content-type",   value = MediaType.APPLICATION_JSON)
    @ClientHeaderParam(name = "Authorization",  value = "Splunk token")
    Response adicionarJsonSplunk(SplunkDTO splunkDTO);
}
