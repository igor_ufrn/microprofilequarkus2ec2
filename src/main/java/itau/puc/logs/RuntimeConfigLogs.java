package itau.puc.logs;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.ws.rs.*;
import java.util.logging.*;

@Path("/exemplosRuntimeConfigLogs")
@Tag(name = "ExemplosRuntimeConfigLogs")
public class RuntimeConfigLogs {

    private static String getLogLevel(Logger logger) {
        for (Logger current = logger; current != null;) {
            Level level = current.getLevel();
            if (level != null)
                return level.getName();
            current = current.getParent();
        }
        return Level.INFO.getName();
    }

    @PUT
    @Path("/{setLoggerToLevel}")
    @Produces("text/plain")
    public String setLoggerToLevel(@QueryParam("logger") String loggerName, @QueryParam("level") String level) {
        Logger logger = Logger.getLogger(loggerName);
        if (level != null && level.length() > 0)
            logger.setLevel(Level.parse(level));
        return getLogLevel(logger);
    }

    @PUT
    @Path("/{setGlobalLoggerToLevel}")
    @Produces("text/plain")
    public String setGlobalLoggerToLevel(@QueryParam("level") String level) {
        Logger logger = Logger.getGlobal();
        if (level != null && level.length() > 0)
            logger.setLevel(Level.parse(level));
        return getLogLevel(logger);
    }

}
