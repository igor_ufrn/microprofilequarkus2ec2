package itau.puc.logs.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SplunkDTO<T> {
    private static final String sourcetype = "httpclient";
    private T event;
}
