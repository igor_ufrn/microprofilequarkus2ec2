package itau.puc.microprofile.jwt;

import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/microprofile-jwt")
@ApplicationScoped
@Tag(name = "ExemplosMicroprofileJWT")
public class ExemplosMicroprofileJWT {


    @Inject
    JsonWebToken token;

    @Inject
    @Claim(value = "sub")
    String nome;

    @Inject
    @Claim(standard = Claims.sub)
    String standardNome;

    @Inject
    @Claim(value = "sub", standard = Claims.sub)
    Instance<String> instanceNome;

    @GET
    @Path("liberado-para-todos")
    @PermitAll
    @Produces(MediaType.TEXT_PLAIN)
    public String liberadoParaTodos() {
        return "liberadoParaTodos";
    }

    @GET
    @Path("negado-para-todos")
    @DenyAll
    @Produces(MediaType.TEXT_PLAIN)
    public String negadoParaTodos() {
        return "negadoParaTodos";
    }



    @GET
    @Path("cliente")
    @RolesAllowed("cliente")
    @Produces(MediaType.TEXT_PLAIN)
    public String cliente() {

        System.out.println("-------------------------------------------------------");
        System.out.println("Claim usuario com property value: " +nome);
        System.out.println("Claim usuario com standard Claim: " +standardNome);
        System.out.println("Claim usuario com instance Claim: " +instanceNome.get());
        System.out.println("--------------------------------------------------------");



        return "Permitiu acesso de cliente\n\n" +
               "Nome: " + token.getName() + "\n\n" +
               "Todos os dados do TOKEN:\n" + token.toString();
    }

    @GET
    @Path("gerente")
    @RolesAllowed("gerente")
    @Produces(MediaType.TEXT_PLAIN)
    public String gerente() {
        return "Permitiu acesso de gerente";
    }



}
