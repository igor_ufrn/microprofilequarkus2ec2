package itau.puc.microprofile.metrics;


import org.eclipse.microprofile.metrics.*;
import org.eclipse.microprofile.metrics.annotation.Metric;
import org.eclipse.microprofile.metrics.annotation.RegistryType;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Random;

@Path("microprofile-metrics")
@ApplicationScoped
@Tag(name = "ExemplosMicroprofileMetrics")
public class ExemplosMicroprofileMetrics {

    private static Integer maiorNumeroGerado = 0;


    @Inject
    @Metric(name = "contador",description = "exermplo de injecao de metrica Counter")
    Counter counter;

    /*Espécie de contador mais sofisticado*/
    @Inject
    @Metric(name = "meter",description = "exermplo de injecao de metrica Meter")
    Meter meter;


    @Inject
    @Metric(name = "timer",description = "exermplo de injecao de metrica Timer")
    Timer timer;

    @Inject
    @Metric(name = "histogram",description = "exermplo de injecao de metrica Histogram")
    Histogram histogram;

    /**
     * Se não definirmos um escopo, será de application.
     */
    @Inject
    @RegistryType(type=MetricRegistry.Type.APPLICATION)
    MetricRegistry metricRegistryApp;

    @Inject
    @RegistryType(type=MetricRegistry.Type.BASE)
    MetricRegistry metricRegistryBase;

    @Inject
    @RegistryType(type=MetricRegistry.Type.VENDOR)
    MetricRegistry metricRegistryVendor;



    @GET
    @Produces(MediaType.TEXT_PLAIN)
    //@Counted(name = "contadorRequisicoes",description = "descricaoDaMetrica", absolute = false)
    //Quantas Threads estão executando no momento
    //@ConcurrentGauge(name = "chamadasEmParaleto",description = "Quantas Threads estao sendo executadas no momento")
    //@Gauge(name = "tamanhoMaiorNumeroGerado", unit = MetricUnits.NONE)
    //@Metered
    //@Timed
    //@SimplyTimed
    public Integer exemplosMicroprofileMetrics() throws InterruptedException {

        counter.inc(10);
        meter.mark(); //Marca a ocorrência de um evento
        //meter.mark(100); //Marca a ocorrência de um determinado número de eventos
        timer.time(); //Declaração de que evento terminou neste momento.
        //Repassa a chamada e o tempo de duração irá ser o tempo decorrido para a execução desta chamada
        //timer.time(Callable<T> event OU Runnable)

        //Mínimo, máximo, média , desvio padrão, faixa de valores por porcentagem
        histogram.update(1000);

        //Para simular chamadas em paralelo
        //Thread.sleep(2000);

        Random rand = new Random(); //instance of random class
        int int_random = Math.abs(rand.nextInt());
        System.out.println("Número gerado = " + int_random);
        if(maiorNumeroGerado < int_random) {
            maiorNumeroGerado = int_random;
        }

        return maiorNumeroGerado;
    }


    @GET
    @Path("criar-metrica")
    @Produces(MediaType.TEXT_PLAIN)
    public String criarMetrica() {
        /**
         * Se passarmos dados de métrica e ela não existir, esta será criada.
         * Se já existir, será devolvida a métrica criada.
         * Veja abaixo no código comentado que podemos usar um builder para criar a métrica a partir dos
         * metadados dela.
         */
        //Metadata metadata = Metadata.builder().withName("exemploRegistryMetricaContador").withUnit("GIGABYTES").build();
        //Counter counter = metricRegistryApp.counter(metadata);
        Counter counter = metricRegistryApp.counter("exemploRegistryMetricaContador");
        counter.inc();
        return "Métrica Counter Registrada = " + counter.getCount();
    }

    @GET
    @Path("deletar-metrica")
    @Produces(MediaType.TEXT_PLAIN)
    public String deletarMetrica() {
        metricRegistryApp.remove("exemploRegistryMetricaContador");
        return "Métrica Deletada";
    }

    /**
     * EXEMPLOS se você quiser gerar suas métricas através de produtores do CDI, como se fossem fábricas.
     * Nestes exemplos utilizei em FIELD mas para METHOD é semelhante!
     * Pode ser necessário criação de qualificadores (@Qualifier) para permitir a injeção
     * da métrica produzida sem causar erros.
     */
    /*
    @javax.enterprise.inject.Produces
    @Metric(name = "countProduces",description = "Metrica com Producecs")
    @ApplicationScoped
    Counter counterProducer =  new Counter() {

        AtomicLong value = new AtomicLong();

        @Override
        public void inc() {
            this.value.incrementAndGet();
        }

        @Override
        public void inc(long l) {
            this.value.addAndGet(l);
        }

        @Override
        public long getCount() {
            return value.longValue();
        }
    };
     */

    /*
    @javax.enterprise.inject.Produces
    @Metric(name = "gaugeProducer",description = "Metrica gauge com Producecs")
    @ApplicationScoped
    org.eclipse.microprofile.metrics.Gauge<Double> gaugeProducer =  new Gauge<Double>() {
        @Override
        public Double getValue() {return 100.0;}
    };
    */


}
