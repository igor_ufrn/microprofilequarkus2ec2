package itau.puc.microprofile.config;

import org.eclipse.microprofile.config.spi.ConfigSource;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

public class ConfigSourceCustomizado implements ConfigSource {

    Map<String, String> propriedades = Collections.singletonMap("valor.minimo.emprestimo", "1500");



    @Override
    public Map<String, String> getProperties() {
        return propriedades;
    }

    @Override
    public Set<String> getPropertyNames() {
        return this.propriedades.keySet();
    }

    @Override
    public String getValue(String propertyName) {
        return propriedades.get(propertyName);
    }
    @Override
    public String getName() {
        return "ConfigSourceCustomizado";
    }

}
