package itau.puc.microprofile.config;

import org.eclipse.microprofile.config.Config;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.config.spi.ConfigSource;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Optional;

@Path("/microprofile-config")
@ApplicationScoped
@Tag(name = "ExemplosMicroprofileConfig")
public class ExemplosMicroprofileConfig {

    @Inject
    protected Config config;

    @Inject
    @ConfigProperty(name = "identificador.projeto.puc", defaultValue = "100")
    protected Integer identificadorProjetoPuc;

    @Inject
    @ConfigProperty(name = "identificador.projeto.puc")
    protected Optional<Integer> identificadorProjetoPucOptional;

    @Inject
    @ConfigProperty(name = "valor.minimo.emprestimo")
    protected Integer valorMinimoEmprestimo;

    @Inject
    @ConfigProperty(name = "conta.corrente.puc")
    protected ContaCorrentePUC contaCorrentePUC;

    @Inject
    @ConfigProperty(name = "valor.minimo.emprestimo")
    Provider<String> valorMinimoEmprestimoProvider;

    @GET
    @Path("valor-minimo-emprestimo")
    @Produces(MediaType.TEXT_PLAIN)
    public String getValorMinimoEmprestimo() {
        return valorMinimoEmprestimo.toString() + " - " + valorMinimoEmprestimoProvider.get();
    }

    @GET
    @Path("conta-corrente-puc")
    @Produces(MediaType.TEXT_PLAIN)
    public String getContaCorrentePUC() {
        return contaCorrentePUC.getNumero();
    }


    @GET
    @Path("identificador-projeto-puc")
    @Produces(MediaType.TEXT_PLAIN)
    public String getIdentificadorProjetoPuc() {
        return identificadorProjetoPuc.toString();
    }

    @GET
    @Path("identificador-projeto-puc-optional")
    @Produces(MediaType.TEXT_PLAIN)
    public String getIdentificadorProjetoPucOptional() {
        return identificadorProjetoPucOptional.toString();
    }

    @GET
    @Path("config-sources")
    @Produces(MediaType.TEXT_PLAIN)
    public String getConfigSources() {
        //customConfigSourceProvider.get();
        //config = ConfigProvider.getConfig();
        Iterable<ConfigSource> configSources = config.getConfigSources();
        StringBuilder sb = new StringBuilder();
        for (ConfigSource configSource : configSources) {
            sb.append("\nNOME DO CONFIG SOURCE: ").append(configSource.getName())
                    .append("\n PRIORIDADE: ").append(configSource.getOrdinal())
                    .append("\n NOMES DAS PROPRIEDADES: ").append(configSource.getPropertyNames());
        }
        return sb.toString();
    }
}
