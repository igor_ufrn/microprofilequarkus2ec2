package itau.puc.microprofile.config;

public class ContaCorrentePUC {

    private String numero;

    public static ContaCorrentePUC of(String str) {
        ContaCorrentePUC conta = new ContaCorrentePUC();
        conta.numero = str;
        return conta;
    }
    public static ContaCorrentePUC valueOf(String str) {
        ContaCorrentePUC conta = new ContaCorrentePUC();
        conta.numero = str;
        return conta;
    }
    public String getNumero() {
        return numero;
    }
}
