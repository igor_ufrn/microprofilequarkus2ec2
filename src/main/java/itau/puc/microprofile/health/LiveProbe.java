package itau.puc.microprofile.health;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Liveness;

import javax.enterprise.context.ApplicationScoped;

@Liveness
@ApplicationScoped
public class LiveProbe  implements HealthCheck {

    @Override
    public HealthCheckResponse call() {
        return HealthCheckResponse.builder()
                .name("Verificando se a aplicação está respondendo em um tempo aceitável")
                .withData("Chave01","Valor01")
                .withData("Chave02","Valor02")
                .status(1==1).build();
        //return HealthCheckResponse.down("Verificando se a aplicação está respondendo em um tempo aceitável");
    }

}
