package itau.puc.microprofile.health;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Liveness;
import org.eclipse.microprofile.health.Readiness;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

/**
 * Este modo de implementar os CHECKS permite centralizar em uma única classe todas as verificações
 */
@ApplicationScoped
public class HealthChecksProducer {

    @Produces
    @Readiness
    public HealthCheck verificaSeAppEstaProntaParaReceberRequisicoes() {
        return () -> HealthCheckResponse.named("verificaSeAppEstaProntaParaReceberRequisicoes").up().build();
    }

    @Produces
    @Readiness
    public HealthCheck verificaReady() {
        return () -> HealthCheckResponse.named("verificaReady").up().build();
    }

    @Produces
    @Liveness
    public HealthCheck verificaSeContainerAppPrecisaSerReiniciado() {
        return () -> HealthCheckResponse.named("verificaSeContainerAppPrecisaSerReiniciado").up().build();
    }

    @Produces
    @Liveness
    public HealthCheck verificaLive() {
        return () -> HealthCheckResponse.named("verificaLive").up().build();
    }

}
