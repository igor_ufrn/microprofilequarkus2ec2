package itau.puc.microprofile.health;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Readiness;

import javax.enterprise.context.ApplicationScoped;

@Readiness
@ApplicationScoped
public class ReadyProbe  implements HealthCheck {

    @Override
    public HealthCheckResponse call() {
        return HealthCheckResponse.builder()
                .name("Verificação do Banco de dados")
                .withData("Chave01","Valor01")
                .withData("Chave02","Valor02")
                .status(1==1).build();
        //return HealthCheckResponse.down("Verificação do Banco de dados");
    }

}
