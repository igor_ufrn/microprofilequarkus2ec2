package itau.puc.microprofile.openApi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Schema(name = "CadastroPessoaDTO")
public class CadastroPessoaDTO {
    @Schema(description = "Nome completo da Pessoa", maxLength = 150)
    private String nome;
    @Schema(minimum = "10")
    private Integer idade;
    @Schema(maxItems = 2)
    private List<itau.puc.microprofile.openApi.EnderecoDTO> enderecos;
}
