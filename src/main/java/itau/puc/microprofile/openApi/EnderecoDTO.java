package itau.puc.microprofile.openApi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EnderecoDTO {
    @Schema(description  = "Nome completo da rua ou avenida")
    private String logradouro;
    @NotNull
    private String cep;
    @Min(1)
    @Max(999999)
    private Integer numero;
}
