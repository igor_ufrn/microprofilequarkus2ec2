package itau.puc.microprofile.openApi;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/pessoas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "ExemplosMicroprofileOpenAPI")
public class ExemplosMicroprofileOpenAPI {

    @POST
    @Operation(
            deprecated = false,
            description = "Cadastra uma nova pessoa",
            operationId = "cadastrarPessoa",
            summary = "Esta operação cadastra uma pessoa no banco de dados do Itaú"
    )
    public itau.puc.microprofile.openApi.CadastroPessoaDTO cadastrar(@RequestBody(description = "Dados da pessoa a ser cadastrada") itau.puc.microprofile.openApi.CadastroPessoaDTO dto) {
        return null;
    }

    @Path("{id}")
    @DELETE
    public Integer deletar(@PathParam("id") Integer id) {
        return id;
    }


    @GET
    @Path("{id}")
    @Operation(
            operationId = "buscarPessoa",
            description = "Buscar Pessoa no banco de dados",
            summary = "Esta operação busca uma pessoa no banco de dados do Itaú"
    )
    @APIResponse(
            responseCode = "200",
            content = @Content(
                        mediaType = "application/json",
                        schema = @Schema(implementation = itau.puc.microprofile.openApi.PessoaDTOOpenAPI.class
                      )
    ))
    @APIResponse(responseCode = "404", description = "Se a pessoa não for encontrada no banco de dados do itaú")
    public Response buscar(@PathParam("id") Integer id) {
        return null;
    }

    @GET
    @Path("all")
    @Operation(
            operationId = "getAllPessoas",
            description = "Retorna todas as pessoas que constam no banco de dados",
            summary = "Esta operação retorna todas as pessoas que constam no banco de dados"
    )
    @APIResponse(
            responseCode = "200",
            content = @Content(
                    mediaType = "application/json",
                    schema = @Schema(type = SchemaType.ARRAY,implementation = itau.puc.microprofile.openApi.PessoaDTOOpenAPI.class)
            ))
    @APIResponse(responseCode = "404", description = "Se a pessoa não for encontrada no banco de dados do itaú")
    public Response getAll(@PathParam("id") Integer id) {
        return null;
    }

    @Path("{id}/enderecos")
    @GET
    public List<itau.puc.microprofile.openApi.EnderecoDTO> buscarEnderecosByPessoa(@PathParam("id") Integer id) {
        return null;
    }


}