package itau.puc.microprofile.openApi;

import org.eclipse.microprofile.openapi.OASFilter;
import org.eclipse.microprofile.openapi.models.Operation;

public class OASFilterOpenApi implements OASFilter {

    @Override
    public Operation filterOperation(Operation operation) {
        operation.setSummary("SUMARIO OASFilterOpenApi");
        return OASFilter.super.filterOperation(operation);
    }

    
    
}
