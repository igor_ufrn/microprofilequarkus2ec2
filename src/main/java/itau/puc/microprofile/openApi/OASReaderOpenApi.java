package itau.puc.microprofile.openApi;

import org.eclipse.microprofile.openapi.OASFactory;
import org.eclipse.microprofile.openapi.OASModelReader;
import org.eclipse.microprofile.openapi.models.OpenAPI;
import org.eclipse.microprofile.openapi.models.info.Info;

/**
 * Existem diversas maneiras de implementar a documentação com OpenApi:
 * 1) Anotações
 * 2) Arquivo de configuração: yaml, yml ou json
 * 3) Programática
 *
 * Como funciona o processamento?
 * 1) Configura propriedades definidas no arquivo de configurações do MP Config
 * 2) Processa OASModelReader
 * 3) Processa arquivos estáticos
 * 4) Processa anotações
 * 5) Processa OASFilter
 *
 * Esta é a forma programática e ela deve implementar a Interface OASModelReader, como o fizemos aqui.
 * Esta forma de implementar permite, por exemplo, que você busque informações externas na
 * construção da documentação de sua API.
*/
public class OASReaderOpenApi implements OASModelReader {


    @Override
	public OpenAPI buildModel() {
        OpenAPI createOpenAPI = OASFactory.createOpenAPI();
        Info info = OASFactory.createInfo();
        info.setTitle("TITLE DE FORMA PROGRAMATICA");
		info.setDescription("DESCRICAO DE FORMA PROGRAMATICA");
        info.setVersion("1.0.0");
        createOpenAPI.setInfo(info);
        return createOpenAPI;
	}
    
}
