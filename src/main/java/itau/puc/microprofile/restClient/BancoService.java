package itau.puc.microprofile.restClient;

import org.eclipse.microprofile.faulttolerance.Fallback;
import org.eclipse.microprofile.rest.client.annotation.ClientHeaderParam;
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders;
import org.eclipse.microprofile.rest.client.annotation.RegisterProvider;
import org.eclipse.microprofile.rest.client.ext.DefaultClientHeadersFactoryImpl;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.*;
import java.util.concurrent.CompletionStage;

/**
 * Esta interface foi criada para mostrar uma boa forma de fazer
 * chamadas a recursos HTTP ou a outros microsserviços
 */
/*Este path será adicionado na chamada ao microsserviço*/
@Path("banco")
/*No arquivo de propriedades fazemos a associação ao endereço do microsserviço (uri)*/
@RegisterRestClient(configKey = "microsservico-banco")
@ClientHeaderParam(name = "ExemploClientHeaderParamNaInterface", value = "10")
/*O cabeçalho a ser propagado é especificado no arquivo de propriedades*/
@RegisterClientHeaders(DefaultClientHeadersFactoryImpl.class)
@RegisterProvider(ExemploCustomRequestFilter.class)
public interface BancoService {

    @POST
    @ClientHeaderParam(name = "ExemploClientHeaderParamNoMetodo", value = "20")
    @ClientHeaderParam(name = "ExemploChamandoMetodo", value = "{exemploChamandoMetodo}")
    itau.puc.microprofile.restClient.PessoaDTO adicionar(
            @HeaderParam("ExemploHeaderParam") String header,
            @QueryParam("idAgencia") Integer idAgencia,
            itau.puc.microprofile.restClient.PessoaDTO pessoaDTO
    );

    @POST
    @ClientHeaderParam(name = "ExemploClientHeaderParamNoMetodo", value = "20")
    @ClientHeaderParam(name = "ExemploChamandoMetodo", value = "{exemploChamandoMetodo}")
    CompletionStage<itau.puc.microprofile.restClient.PessoaDTO> adicionarAssincrono(
            @HeaderParam("ExemploHeaderParam") String header,
            @QueryParam("idAgencia") Integer idAgencia,
            itau.puc.microprofile.restClient.PessoaDTO pessoaDTO);


    /**
     * Método apenas de referência para caso algum método utilize muitos parâmetros
     * @param exemploBeanParam
     * @return
     */
    @GET
    String exemploComBeanParam(@BeanParam ExemploBeanParam exemploBeanParam);


    /**
     * Exemplo de integração dos MP Rest Client e MP Fault Tolerance
     * @return
     */
    @GET
    @Path("pathQueNaoExiste")
    @Fallback(fallbackMethod = "recuperarClienteQualquerDoBanco")
    itau.puc.microprofile.restClient.PessoaDTO recuperarClienteQualquer();

    default itau.puc.microprofile.restClient.PessoaDTO recuperarClienteQualquerDoBanco() {
        itau.puc.microprofile.restClient.PessoaDTO pessoaDTO = new itau.puc.microprofile.restClient.PessoaDTO();
        pessoaDTO.setOperacao(1);
        pessoaDTO.setIdentificador(2);
        pessoaDTO.setNome("Igor Linnik Method Default na Interface");
        pessoaDTO.setValorMaximoEmprestimo(1000);
        return pessoaDTO;
    }

    default String exemploChamandoMetodo(String cabecalho) {
        return cabecalho + "OK";
    }

    /**
     * Classe exemplo apenas de referência para caso algum método utilize muitos parâmetros
     */
    class ExemploBeanParam {
        @HeaderParam("ExemploHeaderParam") String header;
        @QueryParam("idAgencia")Integer idAgencia;
        itau.puc.microprofile.restClient.PessoaDTO pessoaDTO;
    }

}
