package itau.puc.microprofile.restClient;

import io.opentracing.Tracer;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.rest.client.RestClientBuilder;
import org.eclipse.microprofile.rest.client.RestClientDefinitionException;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletionStage;

/**
 * Este recurso foi criado para ser o primeiro a receber a requisição dos clientes. A partir
 * deste são feitas chamadas para outros recursos HTTP ou para outros microsserviços.
 */
@Path("microprofile-rest-client")
@Tag(name = "ExemplosMicroprofileRestCliente")
public class ExemplosMicroprofileRestClient {

    private static final String ENDPOINT = "ENDPOINT INICIAL DA CADEIA";

    @Inject
    @RestClient
    itau.puc.microprofile.restClient.BancoService bancoService;

    @Inject
    itau.puc.microprofile.restClient.ProxyService proxyService;

    @Inject
    Tracer tracer;

    @POST
    public itau.puc.microprofile.restClient.PessoaDTO cadastrar(@QueryParam("idAgencia")Integer idAgencia, itau.puc.microprofile.restClient.PessoaDTO pessoaDTO) {
        exemploSetarLogsSpanAtual();
        return bancoService.adicionar("01",idAgencia,pessoaDTO);
    }




    @POST
    @Path("microsservice-proxy")
    public itau.puc.microprofile.restClient.PessoaDTO cadastrarProxy(@QueryParam("idAgencia")Integer idAgencia, itau.puc.microprofile.restClient.PessoaDTO pessoaDTO) {
        setarBaggageActiveSpanAtual();
        itau.puc.microprofile.restClient.PessoaDTO dadosRetorno = proxyService.adicionarPessoa(idAgencia,pessoaDTO);
        imprimirBaggageItemAposPassarPeloProxy();
        return dadosRetorno;
    }



    @POST
    @Path("cadastrar-de-forma-assincrona")
    public CompletionStage<itau.puc.microprofile.restClient.PessoaDTO> cadastrarDeFormaAssincrona(@QueryParam("idAgencia")Integer idAgencia, itau.puc.microprofile.restClient.PessoaDTO pessoaDTO) {
        return bancoService.adicionarAssincrono("01",idAgencia,pessoaDTO);
    }

    @GET
    @Path("exemplo-sem-injecao-na-classe")
    public String exemploSemInjecaoNaClasse() {
        itau.puc.microprofile.restClient.PessoaDTO pessoa = new itau.puc.microprofile.restClient.PessoaDTO(123, "Ivis", 456,1465);
        itau.puc.microprofile.restClient.BancoService bancoService = CDI.current().select(itau.puc.microprofile.restClient.BancoService.class, RestClient.LITERAL).get();
        return bancoService.adicionar("02",456, pessoa).toString();
    }

    @GET
    @Path("exemplo-tradicional")
    public String exemploTradicional() throws IllegalStateException, RestClientDefinitionException, URISyntaxException {
        itau.puc.microprofile.restClient.PessoaDTO pessoa = new itau.puc.microprofile.restClient.PessoaDTO(56465, "Rosali", 546, 7894);
        itau.puc.microprofile.restClient.BancoService service = RestClientBuilder.newBuilder()
                .baseUri(new URI("http://localhost:8080"))
                .build(itau.puc.microprofile.restClient.BancoService.class);
        return service.adicionar("03",4564, pessoa).toString();
    }

    private void exemploSetarLogsSpanAtual() {
        /*Aqui criamos um log onde a chave é event e o valor é o que é passado no parâmetro, mas podemos passar um mapa de valores*/
        tracer.activeSpan().log(ENDPOINT +  "ExemplosMicroprofileRestClient.cadastrar");
        Map mapaValores = new HashMap<String,String>();
        mapaValores.put("Chave01","Valor01");
        mapaValores.put("Chave02","Valor02");
        /*Exemplo*/
        tracer.activeSpan().log(mapaValores);
    }

    private void imprimirBaggageItemAposPassarPeloProxy() {
        System.out.println("-------------- AQUI ESTAMOS NO ENDPOINT INICIAL -----------------");
        System.out.println("\n\n------------ ITENS BAGGAGE APOS PASSAR PELO PROXY -----------\n\n");
        tracer.activeSpan().context().baggageItems().forEach(System.out::println);
        System.out.println("\n\n---- FIM IMPRESSAO ITENS BAGGAGE APOS PASSAR PELO PROXY ------n\n");
    }

    private void setarBaggageActiveSpanAtual() {
        tracer.activeSpan().setBaggageItem("BAGGAGE-ITEM-ADICIONADO-ENDPOINT-INICIAL","BAGGAGE-VALUE");
    }

}
