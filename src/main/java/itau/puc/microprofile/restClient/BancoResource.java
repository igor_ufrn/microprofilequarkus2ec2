package itau.puc.microprofile.restClient;

import io.opentracing.Tracer;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;


//import io.opentracing.Tracer;

/**
 * Este recurso foi criado para simular um microsserviço denominado banco.
 */
@Path("/banco")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApplicationScoped
@Tag(name = "ExemplosMicroprofileRestCliente")
@Tag(name = "ExemplosSimulandoOutroMicrosservico")
public class BancoResource {

    private static final String ENDPOINT = "ENDPOINT FINAL DA CADEIA - ";

    private final AtomicInteger contadorChamadas = new AtomicInteger();

    @Inject
    Tracer tracer;



    @POST
    public itau.puc.microprofile.restClient.PessoaDTO adicionar(@Context HttpHeaders headers, @QueryParam("idAgencia")Integer idAgencia, itau.puc.microprofile.restClient.PessoaDTO pessoaDTO) {

        tracer.activeSpan().log(ENDPOINT + "BancoResource.adicionar");
        //tracer.activeSpan().setTag("MinhaTAG", "Valor");


        imprimirBaggageItens();


        //Imprimir cabeçalhos
        imprimirCabecalhos(headers);
        //Executar validacoes

        tracer.activeSpan().log(ENDPOINT + "Início do processo de validações");
        executarValidacoes(idAgencia);
        tracer.activeSpan().log(ENDPOINT + "Fim do processo de validações");

        pessoaDTO.setOperacao(contadorChamadas.incrementAndGet());



        sleepThread();
        //tracer.activeSpan().log("Salvo no banco");
        return pessoaDTO;
    }

    private void imprimirBaggageItens() {
        System.out.println("\n\n------------ BAGGAGE ITENS DO BANCO RESOURCE - ULTIMA ETAPA -----------\n\n");
        tracer.activeSpan().context().baggageItems().forEach(System.out::println);
        System.out.println("\n\n---- FIM IMPRESSAO BAGGAGE ITENS DO BANCO RESOURCE - ULTIMA ETAPA -----\n\n");
    }

    private void executarValidacoes(Integer idAgencia) {
        if(idAgencia < 0) {
            throw new NotFoundException("Agencia não encontrada");
        }

        if(idAgencia.equals(0)) {
            throw new RuntimeException("Não é possível salvar ");
        }
    }


/*
    @GET
    public List<PessoaDTO> buscar(@Context HttpHeaders headers, @QueryParam("idUF")Integer idUF) {
        System.out.println("----------------------------------------------");
        System.out.println("BUSCANDO Municipio");
        printHeaders(headers);
        //tracer.activeSpan().log("buscar no banco");
        if(idUF.equals(0)) {
            throw new NotFoundException("UF não encontrada");
        }

        if(idUF.equals(1)) {
            throw new RuntimeException("Ocorreu um erro ao buscar municipio");
        }

        return Collections.emptyList();
    }
*/

    private void imprimirCabecalhos(HttpHeaders headers) {
        MultivaluedMap<String, String> requestHeaders = headers.getRequestHeaders();
        System.out.println("---");
        for (Entry<String, List<String>> entry : requestHeaders.entrySet()) {
            System.out.println("CABECALHO: "+entry.getKey());
            System.out.println("VALOR(ES): "+entry.getValue());
            System.out.println("---");
        }
        System.out.println("-------------------------------------------------------------");
    }

    private void sleepThread() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("----ERRO AO FAZER SLEEP---");
        }
    }

}