package itau.puc.microprofile.restClient;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import java.io.IOException;
import java.util.Collections;

public class ExemploCustomRequestFilter implements ClientRequestFilter{
    @Override
    public void filter(ClientRequestContext requestContext) throws IOException {

        requestContext.getHeaders().put(
                "ExemploCustomRequestFilter",
                Collections.singletonList("RegistradoComRegisterProviderOuRestClientListener"));


    }
}
