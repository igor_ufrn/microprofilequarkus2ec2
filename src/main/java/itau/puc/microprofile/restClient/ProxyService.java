package itau.puc.microprofile.restClient;

import io.opentracing.Tracer;
import org.eclipse.microprofile.opentracing.Traced;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
/*Esta anotação irá criar um novo Span no trace*/
@Traced
public class ProxyService {

    @Inject
    @RestClient
    itau.puc.microprofile.restClient.BancoService bancoService;

    @Inject
    Tracer tracer;

    public itau.puc.microprofile.restClient.PessoaDTO adicionarPessoa(Integer idAgencia, itau.puc.microprofile.restClient.PessoaDTO pessoaDTO) {
        imprimirBaggageInicialProxy();
        setarBaggageActiveSpanAtual();

        tracer.buildSpan("Registrando uma nova pessoa")
                .asChildOf(tracer.activeSpan())
                .withTag("TAG-CUSTOM-01","VALOR01")
                .withTag("TAG-CUSTOM-02","VALOR02")
                .withTag("TAG-CUSTOM-03","VALOR03")
                .start()
                .finish();


        return bancoService.adicionar("01",idAgencia,pessoaDTO);
    }

    private void setarBaggageActiveSpanAtual() {
        tracer.activeSpan().setBaggageItem("BAGGAGE-ITEM-ADICIONADO-PROXY","BAGGAGE-VALUE");
    }

    private void imprimirBaggageInicialProxy() {
        System.out.println("\n\n------------ ITENS BAGGAGE INICIAIS NO PROXY -----------\n\n");
        tracer.activeSpan().context().baggageItems().forEach(System.out::println);
        System.out.println("\n\n---- FIM IMPRESSAO ITENS BAGGAGE INICIAIS NO PROXY ------\n\n");
    }

}
