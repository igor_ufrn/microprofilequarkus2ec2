package itau.puc.microprofile.restClient;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbTypeDeserializer;
import javax.json.bind.annotation.JsonbTypeSerializer;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PessoaDTO {
    private Integer identificador;
    private String nome;
    private Integer valorMaximoEmprestimo;
    private Integer operacao;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getValorMaximoEmprestimo() {
        return valorMaximoEmprestimo;
    }

    public void setValorMaximoEmprestimo(Integer valorMaximoEmprestimo) {
        this.valorMaximoEmprestimo = valorMaximoEmprestimo;
    }

    public Integer getIdentificador() {
        return identificador;
    }

    public void setIdentificador(Integer identificador) {
        this.identificador = identificador;
    }

    public Integer getOperacao() {
        return operacao;
    }

    public void setOperacao(Integer operacao) {
        this.operacao = operacao;
    }

    @Override
    public String toString() {
        return "PessoaDTO [identificador=" + identificador + ", nome=" + nome + ", valorMaximoEmprestimo=" + valorMaximoEmprestimo + ", operacao=" + operacao + "]";
    }


}
