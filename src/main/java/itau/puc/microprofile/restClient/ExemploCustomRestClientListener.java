package itau.puc.microprofile.restClient;

import org.eclipse.microprofile.rest.client.RestClientBuilder;
import org.eclipse.microprofile.rest.client.spi.RestClientListener;

public class ExemploCustomRestClientListener implements RestClientListener{
    @Override
    public void onNewClient(Class<?> serviceInterface, RestClientBuilder builder) {
        builder.register(itau.puc.microprofile.restClient.ExemploCustomRequestFilter.class);
    }

}
