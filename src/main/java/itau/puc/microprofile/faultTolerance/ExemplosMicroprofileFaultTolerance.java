package itau.puc.microprofile.faultTolerance;

import itau.puc.microprofile.restClient.BancoService;
import itau.puc.microprofile.restClient.PessoaDTO;
import org.eclipse.microprofile.faulttolerance.*;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;


@Path("microprofile-fault-tolerance")
@ApplicationScoped
@Tag(name = "ExemplosMicroprofileFaultTolerance")
public class ExemplosMicroprofileFaultTolerance {

    @Inject
    @RestClient
    protected BancoService bancoService;

    private  static final AtomicInteger atomicInteger = new AtomicInteger();

    @GET
    @Path("timeout")
    @Produces(MediaType.TEXT_PLAIN)
    //O padrão é 1 segundo
    @Timeout(200)
    public String timeout(@QueryParam("sleep") Integer sleep) throws InterruptedException {
        Thread.sleep(sleep);
        return "ExemploComTimeout";
    }


    @GET
    @Path("retry")
    @Produces(MediaType.TEXT_PLAIN)
    @Retry(maxRetries = 15)
    public String retry(@QueryParam("valor") Integer valor) throws InterruptedException {
        int valorAtomicInteger = atomicInteger.incrementAndGet();
        System.out.println("Valor Atomic Integer = " + valorAtomicInteger);
        if(valorAtomicInteger < valor) {
            throw new IllegalArgumentException("Valor irregular");
        }
        return "ExemploComRetry - Valor Atomic Integer = " + valorAtomicInteger;
    }


    @GET
    @Path("fallback-method")
    @Produces(MediaType.TEXT_PLAIN)
    @Fallback(fallbackMethod = "methodfallback")
     public String fallbackMethod(@QueryParam("fallback") Boolean fallback) throws Throwable {
         if (fallback) {
             throw new IllegalArgumentException("Erro na operação");
         }
         return "Resposta do método principal";
    }
    /**
     * Deve ter mesmos parâmetros e retorno do método principal
     * @param fallback
     * @return
     */
    public String methodfallback(Boolean fallback) {
        return "Resposta do método de Fallback";
    }


    @GET
    @Path("fallback-class")
    @Produces(MediaType.TEXT_PLAIN)
    @Fallback(value = itau.puc.microprofile.faultTolerance.ClasseDeFallback.class)
    public String fallbackClass(@QueryParam("fallback") Boolean fallback) throws Throwable {
        if (fallback) {
            throw new IllegalArgumentException("Erro na operação");
        }
        return "Resposta do método principal";
    }


    @GET
    @Path("circuit-breaker")
    @Produces(MediaType.TEXT_PLAIN)
    @CircuitBreaker(
            /**
             * Tempo necessário para nova tentativa de execução
             */
        delay = 20000,
            /**
             * Quantidade de requisições com sucesso para fechar o circuito novamente.
            */
        successThreshold = 5,
            /**
             * 25% das requisições com falha irá abrir o circuito.
             * Como requestVolumeThreshold = 4, neste caso apenas uma
             * requisição com falha irá abrir o circuito!
            */
        failureRatio = 0.25,
        requestVolumeThreshold = 4
    )
    public String circuit(@QueryParam("falhar") Boolean falhar) throws Throwable {
        if (falhar) {
            throw new Exception("Ocorreu erro ao processar a operação");
        }
        return "Operação realizada com sucesso!";
    }


    /**
     * Permite execução simultânea de no máximo X requisições concorrentes.
     * @return
     * @throws Throwable
     */
    @GET
    @Path("bulkhead")
    @Produces(MediaType.TEXT_PLAIN)
    @Bulkhead(value = 5)
    public String bulkhead() throws Throwable {
        Thread.sleep(10000);
         return "Exemplo com bulkhead";
    }

    @GET
    @Path("future")
    @Produces(MediaType.APPLICATION_JSON)
    /**
     * Definimos que será executado em Thread separada e devemos retornar CompletionStage ou Future.
     */
    @Asynchronous
    /**
     * Com o Bulkhead limitamos a quantidade de requisições concorrentes. Passou do limite? ERRO!
     * Em conjunto com Asynchronous, podemos definir uma fila de espera. Estourou a fila? Lança erro!
     */
    @Bulkhead(value=3, waitingTaskQueue = 1)
    public Future<String> future() throws Throwable {
        Thread.sleep(10000);
        CompletableFuture<String> f = new CompletableFuture<>();
        f.completeAsync(ExemplosMicroprofileFaultTolerance::funcaoSucesso);
        return f;
    }


    @GET
    @Path("future-sucesso")
    @Produces(MediaType.APPLICATION_JSON)
    @Asynchronous
    @Retry(maxRetries = 3)
    public Future<String> futureSucesso() throws Throwable {
        Thread.sleep(10000);
        CompletableFuture<String> f = new CompletableFuture<>();
        f.completeAsync(ExemplosMicroprofileFaultTolerance::funcaoSucesso);
        return f;
    }

    /**
     * Retornando Future as anotações consideram que houve sucesso na operação.
     * Só consideraria como falha se desse uma exceção ANTES do retorno do método.
     * @return
     * @throws Throwable
     */
    @GET
    @Path("future-falha")
    @Produces(MediaType.APPLICATION_JSON)
    @Asynchronous
    @Retry(maxRetries = 3)
    public Future<String> futureFalha() throws Throwable {
        atomicInteger.incrementAndGet();
        CompletableFuture<String> future = new CompletableFuture<>();
        future.completeAsync(ExemplosMicroprofileFaultTolerance::funcaoFalha);
        return future;
    }


    /**
     * Agora as anotações de Fault Tolerance são ativadas, pois CompletionStage não
     * é interpretado como tarefa realizada.
     * @return
     * @throws Throwable
     */
    @GET
    @Path("future-stage-falha")
    @Produces(MediaType.APPLICATION_JSON)
    @Asynchronous
    @Retry(maxRetries = 3)
    public CompletionStage<String> stageFalha() throws Throwable {
        atomicInteger.incrementAndGet();
        CompletableFuture<String> future = new CompletableFuture<>();
        future.completeAsync(ExemplosMicroprofileFaultTolerance::funcaoFalha);
        return future;
    }

    private static String funcaoSucesso() {
        return "Operação Realizada com Sucesso!!!";
    }

    private static String funcaoFalha() {
        throw new NullPointerException("Erro ao realizar a operação = " + atomicInteger.get());
    }

    /**
     * Exemplo de adição de resiliência em caso de chamada a serviços externos.
     * @return
     */
    @GET
    @Path("exemplo-rest-client")
    @Produces(MediaType.APPLICATION_JSON)
    public PessoaDTO exemploRestClient()  {
        return bancoService.recuperarClienteQualquer();
    }





}
