package itau.puc.microprofile.faultTolerance;

import org.eclipse.microprofile.faulttolerance.ExecutionContext;
import org.eclipse.microprofile.faulttolerance.FallbackHandler;

public class ClasseDeFallback implements FallbackHandler<String> {
    @Override
    public String handle(ExecutionContext executionContext) {
        return "ClasseDeFallback.handle";
    }
}
