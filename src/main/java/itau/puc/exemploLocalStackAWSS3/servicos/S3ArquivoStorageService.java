package itau.puc.exemploLocalStackAWSS3.servicos;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import itau.puc.exemploLocalStackAWSS3.dto.FileInputBody;
import itau.puc.exemploLocalStackAWSS3.utils.InfoOriginaisArquivo;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;

@ApplicationScoped
@Named
public class S3ArquivoStorageService {

    @Inject
    protected AmazonS3 amazonS3;

    public void upload(FileInputBody fileInputBody, InfoOriginaisArquivo infoOriginaisArquivo) throws IOException {

        String bucketS3= fileInputBody.getBucketName();

        if( ! amazonS3.doesBucketExistV2(bucketS3)) {
            amazonS3.createBucket(bucketS3);
        }

        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType(infoOriginaisArquivo.getContentType());

        var putObjectRequest = new PutObjectRequest(
                bucketS3,
                infoOriginaisArquivo.getNome(),
                fileInputBody.getFileUpload())
                .withMetadata(objectMetadata)
                .withCannedAcl(CannedAccessControlList.PublicRead);
        amazonS3.putObject(putObjectRequest);
    }

    public S3Object download(String bucket, String key) throws IOException {
        S3Object s3Object = amazonS3.getObject(bucket,key);
        return s3Object;
    }



}

