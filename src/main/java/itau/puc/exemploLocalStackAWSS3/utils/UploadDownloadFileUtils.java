package itau.puc.exemploLocalStackAWSS3.utils;

import itau.puc.exemploLocalStackAWSS3.dto.FileInputBody;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.ws.rs.core.MultivaluedMap;
import java.util.List;
import java.util.Map;

public class UploadDownloadFileUtils {

    public static final String CONTENT_DISPOSITION = "Content-Disposition";
    public static final String CONTENT_DISPOSITION_ANEXO = "attachment;filename=";
    public static final String CONTENT_DISPOSITION_SEPARATOR = ";";
    public static final String SEPARATOR_VALUE_PROPERTY = "=";
    public static final String FILENAME = "filename";
    public static final String CONTENT_TYPE = "Content-Type";

    public static InfoOriginaisArquivo getInformacoesOriginaisUploadArquivo(MultipartFormDataInput multipartFormDataInput) {
        InfoOriginaisArquivo infoOriginaisArquivo = new InfoOriginaisArquivo();
        Map<String, List<InputPart>> formParts = multipartFormDataInput.getFormDataMap();
        List<InputPart> inPart = formParts.get(FileInputBody.PAYLOAD_ARQUIVO);
        FOR_INPUT_PART : for (InputPart inputPart : inPart) {
            MultivaluedMap<String, String> headers = inputPart.getHeaders();
            String[] contentDispositionHeader = headers.getFirst(CONTENT_DISPOSITION).split(CONTENT_DISPOSITION_SEPARATOR);
            for (String name : contentDispositionHeader) {
                if ((name.trim().startsWith(FILENAME))) {
                    String[] tmp = name.split(SEPARATOR_VALUE_PROPERTY);
                    infoOriginaisArquivo.setNome(tmp[1].trim().replaceAll("\"", ""));
                    break;
                }
            }
            String contentTypeHeader = headers.getFirst(CONTENT_TYPE);
            infoOriginaisArquivo.setContentType(contentTypeHeader);
            if(infoOriginaisArquivo.getContentType()!= null && !infoOriginaisArquivo.getContentType().isBlank()){
                break FOR_INPUT_PART;
            }
        }
        return infoOriginaisArquivo;
    }



}
