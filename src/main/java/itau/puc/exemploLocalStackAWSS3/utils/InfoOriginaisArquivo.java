package itau.puc.exemploLocalStackAWSS3.utils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@Getter
@Setter
@NoArgsConstructor
public class InfoOriginaisArquivo {
    private String nome;
    private String contentType;
}
