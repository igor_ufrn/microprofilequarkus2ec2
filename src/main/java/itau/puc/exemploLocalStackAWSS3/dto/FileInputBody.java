package itau.puc.exemploLocalStackAWSS3.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.jboss.resteasy.annotations.providers.multipart.PartType;

import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.Serializable;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FileInputBody implements Serializable {
    public static final String PAYLOAD_ARQUIVO = "fileUpload";
    public static final String BUCKETNAME = "bucketName";
    @FormParam(FileInputBody.PAYLOAD_ARQUIVO)
    @Schema(type = SchemaType.STRING, format = "binary", required = true)
    public File fileUpload;
    @FormParam(FileInputBody.BUCKETNAME)
    @PartType(MediaType.TEXT_PLAIN)
    @Schema(required = true)
    public String bucketName;
}

