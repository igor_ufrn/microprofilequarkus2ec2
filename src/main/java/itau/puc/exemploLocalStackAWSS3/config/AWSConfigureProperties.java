package itau.puc.exemploLocalStackAWSS3.config;


import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix = "configuracao.aws.s3", namingStrategy = ConfigMapping.NamingStrategy.VERBATIM)
public interface AWSConfigureProperties {
    String accessKeyID();
    String secretAccessKey();
    String awsRegion();
    String serviceEndpoint();
    boolean pathStyleAccessEnabled();
}
