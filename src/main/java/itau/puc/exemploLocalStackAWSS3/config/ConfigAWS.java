package itau.puc.exemploLocalStackAWSS3.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

@ApplicationScoped
public class ConfigAWS {
    @Inject
    AWSConfigureProperties awsConfigureProperties;
    @Produces
    @Named
    public AmazonS3 amazonS3() {
        var credentials = new BasicAWSCredentials(
                awsConfigureProperties.accessKeyID(),
                awsConfigureProperties.secretAccessKey());
        return AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(awsConfigureProperties.serviceEndpoint(),awsConfigureProperties.awsRegion()))
                .withPathStyleAccessEnabled(awsConfigureProperties.pathStyleAccessEnabled())
                .build();
    }
}
