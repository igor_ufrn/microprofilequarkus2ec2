package itau.puc.exemploLocalStackAWSS3.controladores;

import com.amazonaws.services.s3.model.S3Object;
import itau.puc.exemploLocalStackAWSS3.dto.FileInputBody;
import itau.puc.exemploLocalStackAWSS3.servicos.S3ArquivoStorageService;
import itau.puc.exemploLocalStackAWSS3.utils.InfoOriginaisArquivo;
import itau.puc.exemploLocalStackAWSS3.utils.UploadDownloadFileUtils;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;

@Path("/arquivos")
@Tag(name = "ExemplosUsoLocalStackAWS-S3")
public class ArquivosResource {

    @Inject
    S3ArquivoStorageService s3ArquivoStorageService;

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @RequestBody(content = @Content(
            mediaType = MediaType.MULTIPART_FORM_DATA,
            schema = @Schema(type = SchemaType.OBJECT, implementation = FileInputBody.class)))
    public void upload(@MultipartForm MultipartFormDataInput multipartFormDataInput) throws Exception {
        InfoOriginaisArquivo infoOriginaisArquivo = UploadDownloadFileUtils.getInformacoesOriginaisUploadArquivo(multipartFormDataInput);
        FileInputBody fileInputBody = FileInputBody.builder()
                .fileUpload(multipartFormDataInput.getFormDataPart(FileInputBody.PAYLOAD_ARQUIVO, File.class,null))
                .bucketName(multipartFormDataInput.getFormDataPart(FileInputBody.BUCKETNAME, String.class,null)).build();
        s3ArquivoStorageService.upload(fileInputBody,infoOriginaisArquivo);
    }

    @GET
    @Produces(MediaType.WILDCARD)
    public Response download(@QueryParam("bucket") String bucket, @QueryParam("key") String key) throws Exception {
        S3Object s3Object = s3ArquivoStorageService.download(bucket,key);
        Response.ResponseBuilder response = Response.ok(s3Object.getObjectContent().readAllBytes());
        response.header(UploadDownloadFileUtils.CONTENT_DISPOSITION, UploadDownloadFileUtils.CONTENT_DISPOSITION_ANEXO  + s3Object.getKey());
        response.header(UploadDownloadFileUtils.CONTENT_TYPE,s3Object.getObjectMetadata().getContentType());
        return response.build();
    }
}

