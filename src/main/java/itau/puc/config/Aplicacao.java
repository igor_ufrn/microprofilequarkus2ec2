package itau.puc.config;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.info.License;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@OpenAPIDefinition(info =
@Info(title = "Microprofile Quarkus", version = "Quarkus 2.0.1.Final",
        description = "Implementação de exemplos de uso do Microprofile com o runtime Quarkus",
        contact = @Contact(email = "itau@itau.com.br", name = "Itaú", url = "https://www.itau.com.br/" )
        ,license = @License(name = "Licenciado para o banco Itaú", url = "https://www.itau.com.br/")
)
)
@ApplicationPath("/")
public class Aplicacao extends Application {

}
