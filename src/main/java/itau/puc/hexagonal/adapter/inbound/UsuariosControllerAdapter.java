package itau.puc.hexagonal.adapter.inbound;

import itau.puc.hexagonal.dominio.SituacaoUsuario;
import itau.puc.hexagonal.dominio.Usuario;
import itau.puc.hexagonal.dto.UsuarioDtoInput;
import itau.puc.hexagonal.dto.UsuarioDtoOutput;
import itau.puc.hexagonal.portas.inbound.UsuarioServicePort;
import itau.puc.hexagonal.portas.outbound.UsuarioRepositoryPort;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.modelmapper.ModelMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Path("/hexagonal/api/v1/usuarios")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "ExemplosArquiteturaHexagonal")
@ApplicationScoped
public class UsuariosControllerAdapter  {


    private final UsuarioRepositoryPort usuarioRepositoryPort;
    private final ModelMapper modelMapper;
    private final UsuarioServicePort usuarioServicePort;

    public UsuariosControllerAdapter(UsuarioRepositoryPort usuarioRepositoryPort, ModelMapper modelMapper, UsuarioServicePort usuarioServicePort) {
        this.usuarioRepositoryPort = usuarioRepositoryPort;
        this.modelMapper = modelMapper;
        this.usuarioServicePort = usuarioServicePort;
    }


    @GET
    public List<UsuarioDtoOutput> list() {
        var usuarios = usuarioRepositoryPort.findAll();
        if( usuarios != null && usuarios.count() > 0 ) {
            return usuarios.stream().map(usuario -> modelMapper.map(usuario, UsuarioDtoOutput.class)).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }




    @POST
    @Path("/cadastrar")
    public Response cadastrar(@Valid @RequestBody UsuarioDtoInput usuarioDtoInput){
        Usuario usuario = modelMapper.map(usuarioDtoInput,Usuario.class);
        return Response
                .status(Response.Status.CREATED)
                .entity(modelMapper.map(usuarioServicePort.cadastrar(usuario),UsuarioDtoOutput.class))
                .build();
    }




    @PUT
    @Path("/alterar")
    public UsuarioDtoOutput alterar(@Valid @RequestBody UsuarioDtoInput usuarioDtoInput){
        Usuario usuarioSalvo = usuarioServicePort.atualizar(modelMapper.map(usuarioDtoInput,Usuario.class));
        return modelMapper.map(usuarioSalvo, UsuarioDtoOutput.class);
    }


    @DELETE
    @Path("/{login}/excluir")
    public Response excluir(@PathParam(value = "login") String login){
        usuarioServicePort.remover(login);
        return Response.status(Response.Status.NO_CONTENT).build();
    }


    @PUT
    @Path("/{login}/ativar")
    @Consumes(MediaType.WILDCARD)
    public Response ativar(@PathParam(value = "login") String login){
        usuarioServicePort.ativar(login);
        return Response.status(Response.Status.NO_CONTENT).build();
    }


    @PUT
    @Path("/{login}/inativar")
    @Consumes(MediaType.WILDCARD)
    public Response inativer(@PathParam(value = "login") String login){
        usuarioServicePort.desativar(login);
        return Response.status(Response.Status.NO_CONTENT).build();
    }


    @PUT
    @Path("/{login}/alterarsenha")
    @Consumes(value = MediaType.TEXT_PLAIN)
    public Response alterarSenhaUsuarioLogado(@PathParam(value = "login") String login, @NotBlank @RequestBody String novaSenha){
        usuarioServicePort.alterarPropriaSenha(login,novaSenha);
        return Response.status(Response.Status.NO_CONTENT).build();

    }



    @GET
    @Path("/ativos")
    public List<UsuarioDtoOutput> ativos() {
        var usuarios = usuarioRepositoryPort.findBySituacao(SituacaoUsuario.ATIVO);
        if( usuarios != null && !usuarios.isEmpty() ) {
            return usuarios.stream().map(usuario -> modelMapper.map(usuario, UsuarioDtoOutput.class)).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }


    @GET
    @Path("/inativos")
    public List<UsuarioDtoOutput> inativos() {
        var usuarios = usuarioRepositoryPort.findBySituacao(SituacaoUsuario.INATIVO);
        if( usuarios != null && !usuarios.isEmpty() ) {
            return usuarios.stream().map(usuario -> modelMapper.map(usuario, UsuarioDtoOutput.class)).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }


    @GET
    @Path("/{login}")
    public UsuarioDtoOutput usuarioByLogin(@PathParam(value = "login" ) @NotBlank String login) {
        Usuario usuario = usuarioServicePort.getUsuarioByLogin(login);
        return modelMapper.map(usuario, UsuarioDtoOutput.class);
    }






}
