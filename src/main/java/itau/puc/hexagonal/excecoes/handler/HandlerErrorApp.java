package itau.puc.hexagonal.excecoes.handler;

import itau.puc.hexagonal.excecoes.tipos.NegocioException;
import org.eclipse.microprofile.rest.client.ext.ResponseExceptionMapper;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
public class HandlerErrorApp implements ResponseExceptionMapper<NegocioException> {


    @Override
    public NegocioException toThrowable(Response response) {
        System.out.println(response.toString());
        return null;
    }

    @Override
    public int getPriority() {
        return 2147483647;
    }

    @Override
    public boolean handles(int status, MultivaluedMap headers) {
        return true;
    }
}
