package itau.puc.hexagonal.repository;


import itau.puc.hexagonal.dominio.SituacaoUsuario;
import itau.puc.hexagonal.dominio.Usuario;
import itau.puc.hexagonal.portas.outbound.UsuarioRepositoryPort;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class UsuarioRepository implements UsuarioRepositoryPort {

    public Optional<Usuario> findByLogin(String login){
        return findByIdOptional(login);
    }

    public List<Usuario> findBySituacao(SituacaoUsuario situacaoUsuario){
        return find("from Usuario where situacao = ?1",situacaoUsuario).list();
    }





}
