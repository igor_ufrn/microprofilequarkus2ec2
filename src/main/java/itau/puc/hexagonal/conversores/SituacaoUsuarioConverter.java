package itau.puc.hexagonal.conversores;


import itau.puc.hexagonal.dominio.SituacaoUsuario;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class SituacaoUsuarioConverter implements AttributeConverter<SituacaoUsuario, String> {

    @Override
    public String convertToDatabaseColumn(SituacaoUsuario category) {
        if (category == null) {
            return null;
        }
        return category.getCode();
    }

    @Override
    public SituacaoUsuario convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }

        return Stream.of(SituacaoUsuario.values())
                .filter(c -> c.getCode().equals(code))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
