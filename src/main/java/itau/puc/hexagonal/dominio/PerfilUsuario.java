package itau.puc.hexagonal.dominio;

public enum PerfilUsuario {
    GERENTEGCM,PERFILCONSULTA,ARQUITETO,OPERADORGCM,ADMIN
}
