package itau.puc.hexagonal.dominio;


import itau.puc.hexagonal.conversores.SituacaoUsuarioConverter;
import lombok.*;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;


@Entity
@Table(name = "usuario")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Usuario implements PersistDB {
    /**
     * Código Identificador do Usuário.
     */
    @Id
    @Column(name = "login")
    @Size(min = 1, max = 7)
    @NotBlank
    private String login;

    /**
     * Senha do usuário
     */
    @Column(name = "senha")
    @NotBlank
    private String senha;

    /**
     * Nome do usuário
     */
    @Column(name = "nome")
    @NotBlank
    private String nome;

    /**
     * Descrição do Perfil do Usuário.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "perfil")
    @NotNull
    private PerfilUsuario perfil;

    /**
     * Indicador que informa a Situação do Usuário. A - Ativo I - Inativo
     */
    @Column(name = "situacao")
    @NotNull
    @Convert(converter = SituacaoUsuarioConverter.class)
    private SituacaoUsuario situacao;

    /**
     * Data e Hora em que a Operação de Cadastro, Atualização ou Exclusão do Usuário foi realizada.
     */
    @UpdateTimestamp
    @Column(name = "ts_operacao")
    private LocalDateTime dataOperacao;


}
