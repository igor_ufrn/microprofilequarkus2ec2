package itau.puc.hexagonal.dominio;


import javax.validation.*;
import java.util.Set;

public interface PersistDB {

    /**
     * Adicionar bundle de mensagens
     * Retornar as constrainsts para permitir ao chamador a tomada de decisão
     */
    default void validate() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<PersistDB>> violations = validator.validate(this);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }


}
