package itau.puc.hexagonal.config;

import org.modelmapper.ModelMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

@ApplicationScoped
public class BeansDependencyJarConfig {
    @Produces
    public ModelMapper getModelMapper(){
        return new ModelMapper();
    }
}
