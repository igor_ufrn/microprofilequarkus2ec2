package itau.puc.hexagonal.dto;


import itau.puc.hexagonal.dominio.PerfilUsuario;
import itau.puc.hexagonal.dominio.SituacaoUsuario;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Data
public class UsuarioDtoInput {
    @Size(min = 1, max = 7)
    @NotBlank
    private String login;
    @NotBlank
    private String senha;
    @NotBlank
    private String nome;
    @NotNull
    private PerfilUsuario perfil;
    @NotNull
    private SituacaoUsuario situacao;

}
