package itau.puc.hexagonal.negocio;


import itau.puc.hexagonal.dominio.SituacaoUsuario;
import itau.puc.hexagonal.dominio.Usuario;
import itau.puc.hexagonal.excecoes.tipos.NegocioException;
import itau.puc.hexagonal.excecoes.tipos.UsuarioNaoEncontradoException;
import itau.puc.hexagonal.portas.inbound.UsuarioServicePort;
import itau.puc.hexagonal.portas.outbound.UsuarioRepositoryPort;
import itau.puc.hexagonal.repository.UsuarioRepository;
import org.modelmapper.ModelMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

@ApplicationScoped
public class UsuarioService implements UsuarioServicePort {

    private final UsuarioRepositoryPort usuarioRepositoryPort;
    private final ModelMapper modelMapper;

    public UsuarioService(UsuarioRepository usuarioRepositoryPort, ModelMapper modelMapper) {
        this.usuarioRepositoryPort = usuarioRepositoryPort;
        this.modelMapper = modelMapper;
    }

    @Transactional
    public Usuario cadastrar(Usuario usuario) {
        usuario.validate();
        if(usuarioRepositoryPort.findByLogin(usuario.getLogin()).isPresent()) {
            throw new NegocioException("Este login já está sendo utilizado por outro usuário");
        } else {
            usuarioRepositoryPort.persist(usuario);
            return usuario;
        }
    }

    @Transactional
    public Usuario atualizar(Usuario usuario) {
        usuario.validate();
        Usuario usuarioBanco = usuarioRepositoryPort.findByLogin(usuario.getLogin())
                .orElseThrow(() -> new UsuarioNaoEncontradoException(String.format("Este usuário não existe: %s", usuario.getLogin())));
        modelMapper.map(usuario,usuarioBanco);
        usuarioRepositoryPort.persist(usuarioBanco);
        return usuarioBanco;
    }

    @Transactional
    public void remover(String login) {
        Usuario usuarioBanco = usuarioRepositoryPort.findByLogin(login)
                .orElseThrow(() -> new UsuarioNaoEncontradoException(login));
        usuarioRepositoryPort.delete(usuarioBanco);
    }

    @Transactional
    public void ativar(String login) {
        Usuario usuarioBanco = getUsuarioByLogin(login);
        usuarioBanco.setSituacao(SituacaoUsuario.ATIVO);
    }

    @Transactional
    public void desativar(String login) {
        Usuario usuarioBanco = getUsuarioByLogin(login);
        usuarioBanco.setSituacao(SituacaoUsuario.INATIVO);
    }

    @Transactional
    public void alterarPropriaSenha(String login, String senha) {
        Usuario usuarioBanco = getUsuarioByLogin(login);
    }

    public Usuario getUsuarioByLogin(String login) {
        return usuarioRepositoryPort.findByLogin(login)
                .orElseThrow(() -> new UsuarioNaoEncontradoException(String.format("Este usuário não existe: %s", login)));
    }

}
