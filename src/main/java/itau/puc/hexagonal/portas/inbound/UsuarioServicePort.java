package itau.puc.hexagonal.portas.inbound;

import itau.puc.hexagonal.dominio.Usuario;

public interface UsuarioServicePort {
    Usuario cadastrar(Usuario usuario);
    Usuario atualizar(Usuario usuario);
    void remover(String login);
    void ativar(String login);
    void desativar(String login);
    void alterarPropriaSenha(String login, String senha);
    Usuario getUsuarioByLogin(String login);

}
