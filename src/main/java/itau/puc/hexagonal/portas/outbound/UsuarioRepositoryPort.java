package itau.puc.hexagonal.portas.outbound;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import itau.puc.hexagonal.dominio.SituacaoUsuario;
import itau.puc.hexagonal.dominio.Usuario;

import java.util.List;
import java.util.Optional;

public interface UsuarioRepositoryPort extends PanacheRepositoryBase<Usuario,String> {
    Optional<Usuario> findByLogin(String login);
    List<Usuario> findBySituacao(SituacaoUsuario situacaoUsuario);
}
