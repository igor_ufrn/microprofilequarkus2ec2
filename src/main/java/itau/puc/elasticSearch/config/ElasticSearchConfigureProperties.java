package itau.puc.elasticSearch.config;

import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix = "propriedades.configuracoes.elasticsearch", namingStrategy = ConfigMapping.NamingStrategy.VERBATIM)
public interface ElasticSearchConfigureProperties {
    String hostname();
    int port();
    String scheme();
}

