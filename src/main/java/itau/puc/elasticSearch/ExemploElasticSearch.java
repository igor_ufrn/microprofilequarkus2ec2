package itau.puc.elasticSearch;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

@ApplicationScoped
@Tag(name = "ExemplosElasticSearch")
@Path("exemploElasticSearch")
public class ExemploElasticSearch {

    @Inject
    ESService elastic;


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public String inserirIndex(Indice indice) throws IOException {
        Jsonb jsonb = JsonbBuilder.create();
        String jsonString = jsonb.toJson(indice.getValorIndice());
        elastic.index(indice.getNome(), jsonString);
        return "Indice inserido="+indice.toString();
    }
}
