package itau.puc.elasticSearch;

import java.io.IOException;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import itau.puc.elasticSearch.config.ElasticSearchConfigureProperties;
import itau.puc.exemploLocalStackAWSS3.config.AWSConfigureProperties;
import org.apache.http.HttpHost;

import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import org.elasticsearch.common.xcontent.XContentType;

@ApplicationScoped
public class ESService {
    @Inject
    ElasticSearchConfigureProperties elasticSearchConfigureProperties;
    private RestHighLevelClient client;
    /**
     * Quando o Quarkus subir
     * @param startupEvent
     */
    void startup(@Observes StartupEvent startupEvent) {
        client = new RestHighLevelClient(
                RestClient.builder(new HttpHost(elasticSearchConfigureProperties.hostname(), elasticSearchConfigureProperties.port(), elasticSearchConfigureProperties.scheme())));
    }
    /**
     * Quando o quarkus parar
     * @param shutdownEvent
     */
    void shutdown(@Observes ShutdownEvent shutdownEvent) throws IOException {
        client.close();
    }
    /**
     *
     * Envia para o elasticsearch
     *
     * @param index
     * @param json
     */
    public void index(String index, String json) throws IOException {
        org.elasticsearch.action.index.IndexRequest ir = new org.elasticsearch.action.index.IndexRequest(index).source(json, XContentType.JSON);
        client.index(ir, RequestOptions.DEFAULT);

    }
}
