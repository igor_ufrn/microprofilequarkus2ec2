package itau.puc.elasticSearch;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Indice {
    private String nome;
    private ValorIndice valorIndice;
}
