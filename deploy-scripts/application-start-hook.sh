#!/bin/bash

whoami;
sysctl -w vm.max_map_count=262144
cd /home/ec2-user/deploy;
docker-compose down;
docker-compose pull;
docker-compose up -d;